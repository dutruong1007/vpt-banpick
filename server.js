global.__env == process.env.NODE_ENV || 'development';
global.__base = __dirname;
global._ = require('lodash');
global.__config = require(__dirname + "/server/config.js");
global.__async = require('async');
global.__request = require('request');
global.__axios = require('axios');
global._evts = {};
require('dotenv').config()
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const fs = require("fs")
const { exec } = require("child_process");
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
var LOLWebSocket = require("./server/service/lolWebSocket.js");
var LOLBanPickHandle = require("./server/service/lolBanPickHandle.js");
var Vmix = require("./server/service/Vmix.js");
let check = process.argv[2];
global._config = {};
var vmix;
if (check == "check") {
    initial((err, init) => {
        if (err) {
            console.log(err);
        }
        if (init.success) {
            __config.lol.lockfile = init.lockfile;
        }
        else {
            console.log(init.message);
        }
    });
} 
else {
    __async.waterfall([
        (next) => {
            //console.log('here');
            initial((err, init) => {
                if (err) {
                    return next(err);
                }
                if (init.success) {
                    __config.lol.lockfile = init.lockfile;
                    return next();
                }
                return next(init);
            });
        },
        (next) => {
            var lolWebSocket = new LOLWebSocket(__config.lol.lockfile);
            var lolBanPickHandle = new LOLBanPickHandle(lolWebSocket);
    
            if (process.env['VMIX_RUN'] == "true") {
               vmix = new Vmix(process.env['VMIX_HOST']);
            }
            
            lolWebSocket.openListenSocket((data) => {
                //console.log('hix', data)
                lolBanPickHandle.exec(data, (evts) => {
                    _evts = evts;
                    if (vmix) {
                        vmix.exec(evts);
                    }
                    //console.log('evts', evts);
                });
            });
            return next();
        }
    ], (err, result) => {
        if (err) {
            console.log("message", err.message);
        }

        app.use(express.static('static'))

        app.use(bodyParser());

        const routes = [{
            path: '/',
            method: 'GET',
            middlewares: [(req, res) => {
               // return res.send("OK");
                const html = fs.readFileSync("./index.html", "utf-8");
                return res.send(html);
            }]
        }, {
            path: '/evts',
            method: "GET",
            middlewares: [(req, res) => {
                return res.json({data: _evts})
            }]
        }, {
            path: '/init_vmix',
            method: "GET",
            middlewares: [(req, res) => {
                vmix.initVmixScreen();
                return res.send("OK");
            }]
        }];

        

        routes.forEach(route => {
            applyRoute(app, route);
        });

        const http = require('http').Server(app);

        http.listen("9135", "", function(){
            
            console.log("SERVER LISTENING ON http://localhost:%s", 9135);
            // exec("start http://localhost:9135", (error, stdout, stderr) => {
            // });
        });
    });
}

function initial(cb) {
    exec("WMIC PROCESS WHERE name='LeagueClientUx.exe' GET commandline", (error, stdout, stderr) => {
        if (error) {
            //console.log(`error: ${error.message}`);
            return cb({message: error.message});
        }
        if (stderr) {
            //console.log(`stderr: ${stderr}`);
            return cb({stderr: stderr, message: 'Please open LOL before run this app'});
        }
        //console.log(`stdout: ${stdout}`);
    
        let lockfile = `${stdout.split('LeagueClientUx.exe')[0].split('CommandLine')[1].trim()}`;
    
        if (!lockfile.length) {
            return cb(null, {success: false, message: 'Please run by Adminstator & turn on the LOL before run this app.'});
        }
        lockfile += "lockfile";
        console.log('lockfile_done');
        return cb(null, {success: true, message: "OK", lockfile: lockfile});
    }); 
}


function applyRoute(app, route) {
    var args = _.flatten([route.path, route.middlewares]);

    switch (route.method.toUpperCase()) {
        case "GET":
            app.get.apply(app, args);
            break;
        case "POST":
            app.post.apply(app, args);
            break;
        case "PUT":
            app.put.apply(app, args);
            break;
        case "DELETE":
            app.delete.apply(app, args);
            break;
        default:
            throw new Error('Invalid HTTP method specified for route ' + route.path);
            break;
    }
}







