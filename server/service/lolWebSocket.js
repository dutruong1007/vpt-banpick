const fs = require("fs");
const WebSocket = require('ws');
const request = require("request");
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const MESSAGE_TYPES = {
    WELCOME: 0,
    PREFIX: 1,
    CALL: 2,
    CALLRESULT: 3,
    CALLERROR: 4,
    SUBSCRIBE: 5,
    UNSUBSCRIBE: 6,
    PUBLISH: 7,
    EVENT: 8
};

class RiotWSProtocol extends WebSocket {

    constructor(url) {
        super(url, 'wamp');

        this.session = null;
        this.on('message', this._onMessage.bind(this));
    }

    close() {
        super.close();
        this.session = null;
    }

    terminate() {
        super.terminate();
        this.session = null;
    }

    subscribe(topic, callback) {
        super.addListener(topic, callback);
        this.send(MESSAGE_TYPES.SUBSCRIBE, topic);
    }

    unsubscribe(topic, callback) {
        super.removeListener(topic, callback);
        this.send(MESSAGE_TYPES.UNSUBSCRIBE, topic);
    }

    send(type, message) {
        super.send(JSON.stringify([type, message]));
    }

    _onMessage(message) {
        const [type, ...data] = JSON.parse(message);

        switch (type) {
            case MESSAGE_TYPES.WELCOME:
                this.session = data[0];
                // this.protocolVersion = data[1];
                // this.details = data[2];
                break;
            case MESSAGE_TYPES.CALLRESULT:
                console.log('Unknown call, if you see this file an issue at https://discord.gg/hPtrMcx with the following data:', data);
                break;
            case MESSAGE_TYPES.TYPE_ID_CALLERROR:
                console.log('Unknown call error, if you see this file an issue at https://discord.gg/hPtrMcx with the following data:', data);
                break;
            case MESSAGE_TYPES.EVENT:
                const [topic, payload] = data;
                this.emit(topic, payload);
                break;
            default:
                console.log('Unknown type, if you see this file an issue with at https://discord.gg/hPtrMcx with the following data:', [type, data]);
                break;
        }
    }
}

class LOLWebSocket {
    constructor(lockfile_path) {
        this._lockfile_path = lockfile_path;
        var lf = fs.readFileSync(this._lockfile_path, 'utf-8');
        var splits = lf.split(":");
        this._port = splits[2];
        this._pass = splits[3];
        this._host = `https://riot:${this._pass}@127.0.0.1:${this._port}`;
        this._ws = new RiotWSProtocol(`wss://riot:${this._pass}@localhost:${this._port}/`);
        this.getAllChampions();
        this._self = this;
    }

    getAllChampions() {
        // __request.get(`${self.lol.host}/lol-champ-select/v1/all-grid-champions`, function(err, result, body) {
        //     Champs = JSON.parse(body); 
        // });
        let self = this;
        request({
            url: `${self._host}/lol-champ-select/v1/all-grid-champions`,
            method: 'get',
            json: true
        }, function(err, result, body) {
            //console.log('champions', body)
            self._champions = body; 
        });
    }

    getHost() {
        return this._host;
    }

    getChamps() {
        return this._champions;
    }

    openListenSocket(cb) {
        let self = this;
        self._ws.on('open', () => {
            console.log('websocket listen')
            self._ws.subscribe('OnJsonApiEvent', function (data) { 
             //   console.log('da', data);
                cb(data);
            })
        })
    }
}

module.exports = LOLWebSocket;