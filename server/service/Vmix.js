const request = require("request");
var parseString = require('xml2js').parseString;

//slotID: tu 0 den 9.
const cfg = {
    
    inputs: {
        'ban overlay.gtzip': null,
        'pick overlay.gtzip': null,
        'Picking': null,
        'Banning': null,
        'Banned': null,
        'Picked': null
    },
    time_count: {
        input: 'ban overlay.gtzip',
    },
    picking: {
        input: 'Picking',
    },
    banning: {
        input: 'Banning',
    },
    mov_banned: {
        input: 'Banned',
    },
    mov_picked: {
        input: 'Picked'
    },
    banned: {
        input: 'ban overlay.gtzip',
    },
    players: {
        name: {
            input: 'pick overlay.gtzip',
        },
        champion: {
            input: 'pick overlay.gtzip'
        },
        spell: {
            input: 'pick overlay.gtzip'
        }
    }
}

class Vmix {

    constructor(host) {
        this._host = host;
        let self = this;
        this._timeout_arr = [];
        this.fetchVmixInput(() => {
            for (let i in cfg.inputs) {
                cfg.inputs[i] = self.findInputByTitle(i);
                console.log('iii', i, cfg.inputs[i]);
            }
           
        });
    }

    fetchVmixInput(cb) {
        let self = this;
        request({
            url: self._host,
            type: 'get'
        }, (err, res, body) => {
            //console.log('er', body)
            parseString(body, function(err, data) {
                self._vmix = data.vmix;
               // console.log('fr', self._vmix)
                cb();
            });
        })
    }

    findInputByTitle(title) {
        for (var item of this._vmix.inputs[0].input) {
            if (item.$.title == title) {
                return item.$.number;  
            }
        }
    }

    sendGetRequest(url, retry = false) {
        let self = this;
       // console.log('url', encodeURI(url))
        request({
            uri: encodeURI(url),
            method: 'GET'
        }, (err, res, body) => {
            if (err) {
                console.log("ERR Request", url);
                //retried
                if (retry) {
                    self.sendGetRequest(url);
                }
                 
            }
        });
    }

    setImage(input, value, selected_name, retry) {
        let url = `${this._host}/?Function=SetImage&Value=${value}&Input=${input}&SelectedName=${selected_name}`;
        this.sendGetRequest(url, retry);
    }

    setText(input, text, selected_name) {
       // console.log('set Text', input, text, selected_name, encodeURI(`${this._host}/?Function=SetText&Value=${text}&Input=${input}&SelectedName=${selected_name}`) );
        let url = `${this._host}/?Function=SetText&Value=${text}&Input=${input}&SelectedName=${selected_name}`;
        this.sendGetRequest(url);
    }

    setMultiViewOverlayOff(input, value, retry) {
        let url = `${this._host}/?Function=MultiViewOverlayOff&Value=${value}&Input=${input}`;
        this.sendGetRequest(url, retry);
    }

    setMultiViewOverlayOn(input, value, retry) {
        let url = `${this._host}/?Function=MultiViewOverlayOn&Value=${value}&Input=${input}`;
        this.sendGetRequest(url, retry);
    }

    setActioning() {
        if (!this._actioning) return;

    }

    initVmixScreen() {
        let self = this;
        for (let i = 0; i < 10; i++) {
            //set Playername
            self.setText(cfg.inputs[cfg.players.name.input], "", `p name ${i}.Text`);
            ///set Spell
            self.setImage(cfg.inputs[cfg.players.spell.input], '', `spell ${i}a.Source`);
            self.setImage(cfg.inputs[cfg.players.spell.input], '', `spell ${i}b.Source`);
           //set Champion
            self.setImage(cfg.inputs[cfg.players.champion.input], '', `Champ ${i}.Source`);

            self.setImage(cfg.inputs[cfg.banned.input], '', `ban ${i}.Source`);

               // off banning
            self.setMultiViewOverlayOff(cfg.inputs[cfg.banning.input], `${i + 1}`);
            self.setMultiViewOverlayOff(cfg.inputs[cfg.mov_banned.input], `${i + 1}`);
            //off picking
            self.setMultiViewOverlayOff(cfg.inputs[cfg.picking.input], `${i + 1}`);
            self.setMultiViewOverlayOff(cfg.inputs[cfg.mov_picked.input], `${i + 1}`);
        }
    }

    exec(evts) {
        let self = this;
        if (evts.action_active && evts.action_active.type == "start_game") {
            //new ban/pick phase.
            this._bannings = [];
            this._pickings = [];
            this._actioning = null;
            this._timeout_arr = [];
            //set Player name
            self._players = evts.players;
           // console.log('start gameeeee', self._players)
            for (let i in self._players) {
                //set Playername
                self.setText(cfg.inputs[cfg.players.name.input], self._players[i].data.summonerName, `p name ${self._players[i].index}.Text`);
                ///set Spell
                this.setImage(cfg.inputs[cfg.players.spell.input], '', `spell ${self._players[i].index}a.Source`);
                this.setImage(cfg.inputs[cfg.players.spell.input], '', `spell ${self._players[i].index}b.Source`);
               //set Champion
                this.setImage(cfg.inputs[cfg.players.champion.input], '', `Champ ${self._players[i].index}.Source`);
            }

            for (let i = 0; i < 10; i++) {
               // set banned
                this.setImage(cfg.inputs[cfg.banned.input], '', `ban ${i}.Source`);

               // off banning
                this.setMultiViewOverlayOff(cfg.inputs[cfg.banning.input], `${i + 1}`);

                //off picking
                this.setMultiViewOverlayOff(cfg.inputs[cfg.picking.input], `${i + 1}`);
            }

            return;
        }
        let nnn = self._timeout_arr.length;
        if (nnn) {
            for (let i = nnn-1; i >=0; i--) {
                if (Date.now() - self._timeout_arr[i].ts > self._timeout_arr[i].second) {
                    self.setMultiViewOverlayOff(self._timeout_arr[i].input, self._timeout_arr[i].index, true);
                    self._timeout_arr.splice(i, 1);
                }
            }
        }

        if (evts.action_active && evts.action_active.time_left) {
            if (!self._timecount || self._timecount.ts != evts.action_active.timestamp) {
               // console.log('change time phase', self._timecount && self._timecount.ts, evts.action_active.timestamp)
                self._timecount = {count: evts.action_active.time_left, ts: evts.action_active.timestamp};
                if (self._timecountInterval) {
                    clearInterval(self._timecountInterval);
                }
                if (self._timecount.count) {
                    let count = self._timecount.count - (Date.now() - self._timecount.ts);
                    count = parseInt(count/1000);
                    if (count < 0) self._count = 0;
                    if (count != self._count) {
                        self._count = count;
                        if (count < 0) {
                            self._count = 0;
                        }
                        self.setText(cfg.inputs[cfg.time_count.input], self._count, 'time countdown.Text');
                    }
                }
                self._timecountInterval = setInterval(function() {
                    let nnn = self._timeout_arr.length;
                    if (nnn) {
                        for (let i = nnn-1; i >=0; i--) {
                            if (Date.now() - self._timeout_arr[i].ts > self._timeout_arr[i].second) {
                                self.setMultiViewOverlayOff(self._timeout_arr[i].input, self._timeout_arr[i].index, true);
                                self._timeout_arr.splice(i, 1);
                            }
                        }
                    }
                    if (self._timecount.count) {
                        let count = self._timecount.count - (Date.now() - self._timecount.ts);
                        count = parseInt(count/1000);
                        if (count < 0) self._count = 0;
                        if (count != self._count) {
                            self._count = count;
                            if (count < 0) {
                                self._count = 0;
                                clearTimeout(self._timecountInterval)
                            }
                            self.setText(cfg.inputs[cfg.time_count.input], self._count, 'time countdown.Text');
                        }
                    }
                }, 1000);
            }
            
        }
    

        self._players = evts.players;
        self._bans = evts.ban;

        if (evts.action_active ) {
            if (evts.action_active.type == "banning") {
                console.log('banning', self._actioning, evts.action_active)
                if (!self._actioning) {
                    self._actioning = {
                        index: evts.action_active.index,
                        type: 'ban'
                    }
                    self.setMultiViewOverlayOn(cfg.inputs[cfg.banning.input], `${parseInt(self._actioning.index) + 1}`, true);
                }
            }
            else if (evts.action_active.type == "banned") {
                if (self._actioning && evts.action_active.index == self._actioning.index) { // 
                    let vl = parseInt(self._actioning.index) + 1;
                    self.setMultiViewOverlayOff(cfg.inputs[cfg.banning.input], vl, true);
                    self.setMultiViewOverlayOn(cfg.inputs[cfg.mov_banned.input], vl, true);
                    self._timeout_arr.push({input: cfg.inputs[cfg.mov_banned.input], index: vl, second: 500, ts: Date.now()});
                    self._actioning = null;

                }
                for (let i in self._bans) {
                    if (self._bans[i].champ) {
                        self.setImage(cfg.inputs[cfg.banned.input], `${process.env.STATIC_FOLDER}/static/champions_square/${self._bans[i].champ.name || 'null'}.png`, `ban ${self._bans[i].index}.Source`);
                    }
                }
                
            }
            else if (evts.action_active.type == "picking") {
                if (!self._actioning || (self._actioning && self._actioning.type =='ban' && self._actioning.index == 9)) {
                    self._actioning = {
                        index: evts.action_active.index,
                        type: 'pick'
                    }
                    self.setMultiViewOverlayOn(cfg.inputs[cfg.picking.input], `${parseInt(self._actioning.index) + 1}`, true);
                }
                if (evts.action_active.champ) {
                    self.setImage(cfg.inputs[cfg.players.champion.input], `${process.env.STATIC_FOLDER}/static/champions/${evts.action_active.champ && evts.action_active.champ.name || 'null'}.jpg`, `Champ ${evts.action_active.index}.Source`)
                }
                    
            }
            else if (evts.action_active.type == "picked") {
                if (self._actioning && self._actioning.type == 'pick' && evts.action_active.index == self._actioning.index) {
                    let vl1 = parseInt(self._actioning.index) + 1;
                    self.setMultiViewOverlayOff(cfg.inputs[cfg.picking.input], vl1);
                    self.setMultiViewOverlayOn(cfg.inputs[cfg.mov_picked.input], vl1);
                    self._timeout_arr.push({input: cfg.inputs[cfg.mov_picked.input], index: vl1, second: 500, ts: Date.now()});
                    self._actioning = null;
                }

                //setTimeout(function() {
                for (let i in self._players) {
                    if (self._players[i].champ) {
                        self.setImage(cfg.inputs[cfg.players.champion.input], `${process.env.STATIC_FOLDER}/static/champions/${self._players[i].champ.name}.jpg`, `Champ ${self._players[i].index}.Source`, true);
                    }
                }
               // }, 10);
                
            }
            else if (evts.action_active.type == "change_spell") {
               // console.log('change spell', evts.players)
                self._players = evts.players;
                for (let i in self._players) {
                    //set Spell
                    if (self._players[i].index == evts.action_active.index) {
                        if (self._players[i].spells) {
                            self.setImage(cfg.inputs[cfg.players.spell.input], `${process.env.STATIC_FOLDER}/static/spells/${self._players[i].spells[0]}`, `spell ${self._players[i].index}a.Source`);
                            self.setImage(cfg.inputs[cfg.players.spell.input], `${process.env.STATIC_FOLDER}/static/spells/${self._players[i].spells[1]}`, `spell ${self._players[i].index}b.Source`);
                        }
                    }
                    
                }
    
            }
            else if (evts.action_active.type == 'FINAL') {
                self._actioning = null;
                for (let i = 0; i < 10; i++) {
                    // off banning
                     this.setMultiViewOverlayOff(cfg.inputs[cfg.banning.input], `${i + 1}`);
                     //off picking
                     this.setMultiViewOverlayOff(cfg.inputs[cfg.picking.input], `${i + 1}`);
                }
            }
            else if (evts.action_active.type == 'JOIN_GAME') {
              
                for (let i in self._players) {
                    if (self._players[i].champ) {
                        self.setImage(cfg.inputs[cfg.players.champion.input], `${process.env.STATIC_FOLDER}/static/champions/${self._players[i].champ.name}.jpg`, `Champ ${self._players[i].index}.Source`);
                    }
                    if (self._players[i].spells) {
                        self.setImage(cfg.inputs[cfg.players.spell.input], `${process.env.STATIC_FOLDER}/static/spells/${self._players[i].spells[0]}`, `spell ${self._players[i].index}a.Source`);
                        self.setImage(cfg.inputs[cfg.players.spell.input], `${process.env.STATIC_FOLDER}/static/spells/${self._players[i].spells[1]}`, `spell ${self._players[i].index}b.Source`);
                    }
                }
            }
        }

    }
}


module.exports = Vmix;