

const request = require("request");

class LOLBanPickHandle {

    //players : {0: {data, champion, spell}};0 -> 9

    //bans: {0 - > 9}

    //active_player : {index: 0, timeleft: 27, timeleft_date = 2021/10/10, champ_active}

    //active_ban: {index: 0, timeleft: 26, timeleft_date, champ_active}

    constructor(lolWebSocket) {
        this._lolWebSocket = lolWebSocket;
        this._players = {}
        this._bans = {}
    }

    findChamp(id) {
        if (id == 0) return null;
        let champs = this._lolWebSocket.getChamps();
        for (var item of champs) {
            if (item.id == id) {
                return {id: id, name: item.name, pic: item.squarePortraitPath};
            }
        }
        return {};
    }

    exec(data, cb) {
        let self = this;

       // console.log('da', data);
        
        if (data.uri == "/lol-champ-select-legacy/v1/pickable-champion-ids" && data.eventType == "Create") {
            //start ban pick

            self._bans = {};
            self._players = {};
            self._active_ban = self._active_pick = null;
    
            request({
                url: encodeURI(self._lolWebSocket.getHost() + "/lol-lobby/v2/lobby"),
                method: 'get',
                json: true
            }, function(err, res, body) {
              //  console.log('lobby', body);
                if (body.gameConfig && body.gameConfig.customTeam100) {
                    for (let index in body.gameConfig.customTeam100) {
                        self._players[`player_` + index] = {index: index};
                        self._players[`player_` + index].data = body.gameConfig.customTeam100[index];
                    }
                }

                if (body.gameConfig && body.gameConfig.customTeam200) {
                    for (let index in body.gameConfig.customTeam200) {
                        self._players[`player_` + (parseInt(index) + 5)] = {index: (parseInt(index) + 5)};
                        self._players[`player_` + (parseInt(index) + 5)].data = body.gameConfig.customTeam200[index];
                    }
                }


                if (!self._action_active) {
                    self._action_active = {};
                }

                self._action_active.type = "start_game";
                
              //  console.log('start game', self._players);

                cb({ban: self._bans, players: self._players, action_active: self._action_active});

                //console.log('players', self._players)
            })
        }

        else if (data.uri == '/lol-champ-select-legacy/v1/session') {
            
            if (data.data && data.data.timer && data.data.timer.phase == "GAME_STARTING") {
                this._action_active = {type: 'JOIN_GAME', time_left: data.data.timer.adjustedTimeLeftInPhase, timestamp: data.data.timer.internalNowInEpochMs}
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
            }

            if (data.data && data.data.timer && data.data.timer.phase == "FINALIZATION") {
                //finish ban pick
                this._action_active = {type: 'FINAL', time_left: data.data.timer.adjustedTimeLeftInPhase, timestamp: data.data.timer.internalNowInEpochMs}
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
            }

            if (data.data && data.data.timer && data.eventType != 'Create') {

                //get active_action:

                
                let actions = data.data.actions;

                for (let a of actions) {
                    for (let ad of a) {
                        if (!ad.completed && ad.isInProgress == true) {
                            //this._action_active = a;
                            if (this._action_active && ad.id > this._action_active.id) {
                                this._last_action = JSON.parse(JSON.stringify(this._action_active || {}));
                            }
                            this._action_active = ad;
                            this._action_active.time_left = data.data.timer.adjustedTimeLeftInPhase;
                            this._action_active.timestamp = data.data.timer.internalNowInEpochMs;
                            if (ad.type == "ban") {
                               let banningChamp = self.findChamp(ad.championId);
                             //  console.log('in banning', this._action_active);
                               this._action_active.champ = banningChamp;
                               this._action_active.type = 'banning';
                               let i = this._bans['baned_5'] ? 5 : -1;
                               this._action_active.index = this._action_active.pickTurn + i;
                               cb({ban: this._bans, players: this._players, action_active: this._action_active});
                            }
                            else if (ad.type == "pick") {
                                let selectChamp = self.findChamp(ad.championId);
                                this._action_active.champ = selectChamp;
                                this._action_active.type = 'picking';
                                this._action_active.index = this._action_active.actorCellId;
                             //   console.log('In selecting champ ',  selectChamp);
                                cb({ban: this._bans, players: this._players, action_active: this._action_active});
                            }
                        }
                        if (ad.completed) {
                        }
                    }
                }
            }
        }   

        else if (/\/lol-champ-select\/v1\/summoners\/(\d+)/.test(data.uri)) {
            if (data.data && data.data.statusMessageKey == "banning_champion") {
               // console.log('data', data.data);4
                //    console.log('bb', data)
                //     let banning = /\/lol-game-data\/assets\/v1\/champion-icons\/(\d+)/g.exec(data.data.banIntentSquarePortratPath);

            }

            else if (data.data.championName && data.data.championIconStyle) {
                var pick = /\/lol-game-data\/assets\/v1\/champion-icons\/(\d+)/g.exec(data.data.championIconStyle)[1];
                var activePick = this.findChamp(pick);
                if (data.data.isActingNow && !data.data.isDonePicking) {
                
                }
                else if (!data.data.isActingNow && data.data.isDonePicking) {
                    if (activePick)
                        activePick.status = 'picked';
                    if (this._players['player_' + data.data.slotId]) {
                        this._players['player_' + data.data.slotId].champ = activePick
                        this._players['player_' + data.data.slotId].index = data.data.slotId
                        // this._players['player_' + this._action_active.actorCellId].index = this._action_active.actorCellId;
                        console.log('Picked champ ', activePick, data.data.slotId);
                        this._action_active.type = 'picked';
                        this._action_active.index = data.data.slotId;
                        cb({ban: this._bans, players: this._players, action_active: this._action_active});
                    }
                }
            }

            //GAME_STARTING
            ///lol-gameflow/v1/session

            if (data.data.spell1IconPath) {  
                let spellName1 = /\/lol-game-data\/assets\/DATA\/Spells\/Icons2D\/(.+)/g.exec(data.data.spell1IconPath)[1];
                let spellName2 = /\/lol-game-data\/assets\/DATA\/Spells\/Icons2D\/(.+)/g.exec(data.data.spell2IconPath)[1];
                for (let k in this._players) {
                    if (this._players[k].index == data.data.slotId) {
                        if (!this._players[k].spells) this._players[k].spells = ["", ""];
                        this._players[k].spells[0]  = spellName1;
                        this._players[k].spells[1]  = spellName2;
                        this._action_active.index = data.data.slotId;
                    }
                }
                if (this._action_active)
                    this._action_active.type = 'change_spell';
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
            }
               
        }

        else if (data.uri == 'lol-gameflow/v1/session') {
            //console.log('d', JSON.stringify(data));
        }

        else if (data.uri == '/lol-champ-select/v1/sfx-notifications') {
            if (data.data[0] && data.data[0].eventType == 'champion-ban-vo') {
                // console.log(data.data[0].path);
                var ban = /\/lol-game-data\/assets\/v1\/champion-ban-vo\/(\d+)/.exec(data.data[0].path)[1];
                var banChamp = self.findChamp(ban);
                console.log('banned '+ banChamp.name, this._action_active);
                this._action_active.type = 'banned';
                this._action_active.champ = banChamp;
                let i = this._bans['baned_5'] ? 5 : -1;
                this._action_active.index = this._action_active.pickTurn + i;
                
                this._bans['baned_' + (this._action_active.pickTurn + i)] = {champ: banChamp, index: (this._action_active.pickTurn + i)};
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
                
            }
            if (data.data[0] && data.data[0].eventType == "serial-bans-enemy-ban-completed") {
                console.log("banned enemy", this._action_active, this._last_action);
               // console.log("banned team", this._action_active, this._last_action);
                let i = this._bans['baned_5'] ? 5 : -1;
                this._action_active.type = 'banned';
                this._action_active.index = this._last_action.pickTurn + i;
               // console.log("not ban", this._action_active);
                this._bans['baned_' + (this._last_action.pickTurn + i)] = {champ: null, index: (this._last_action.pickTurn + i)};
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
                
            }
            else if (data.data[0] && data.data[0].eventType == 'serial-bans-ally-ban-completed') {
                console.log("banned team", this._action_active, this._last_action);
                let i = this._bans['baned_5'] ? 5 : -1;
                this._action_active.type = 'banned';
                this._action_active.index = this._last_action.pickTurn + i;
               // console.log("not ban", this._action_active);
                this._bans['baned_' + (this._last_action.pickTurn + i)] = {champ: null, index: (this._last_action.pickTurn + i)};
                cb({ban: this._bans, players: this._players, action_active: this._action_active});
               // console.log('bans ally', this._bans); //not ban
            }
            if (data.data[0] && data.data[0].eventType == 'draft-pick-locked-in-their-team-single' || data.data[0] && data.data[0].eventType == 'draft-pick-locked-in-my-team-single') {
               // console.log('picked', this._active_pick);
            }
        }

    }
}

module.exports = LOLBanPickHandle;