#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here


#include <MsgBoxConstants.au3>
#include <Constants.au3>
#include <GUIConstantsEx.au3>
#RequireAdmin

Dim $log, $output

start();

Func stop()
	Exit;
EndFunc

Func start()

	ConsoleWrite("---RUN APP---"  & @LF)

	GUICreate("VPT Ban/Pick Tool", 180, 100)
	GUICtrlCreateLabel("VPT BAN/PICK TOOL", 30, 10)
	#Local $idButton_Close = GUICtrlCreateButton("STOP", 70, 50, 60)
	GUISetState(@SW_SHOW)

	$log = Run(@ComSpec & " /c node -v", '', @SW_HIDE, $STDERR_CHILD + $STDOUT_CHILD)
	ProcessWaitClose($log)
	$output = StdoutRead($log)
	ConsoleWrite("output:" & $output)

	If ($output == "") Then
		MsgBox($MB_OK, "Message", "Please install nodejs at https://nodejs.org/en/download/ before!")
		Exit;
		Return;
	Else
		#MsgBox($MB_OK, "Message", "Nodejs installed")
	EndIf

	$log_node = Run(@ComSpec & " /c node server check", '', @SW_HIDE, $STDERR_CHILD + $STDOUT_CHILD)
	ProcessWaitClose($log_node)
	$node_output = StdoutRead($log_node)
	ConsoleWrite("node output: " & $node_output)
	if (StringInStr($node_output, "lockfile_done")) Then
		#MsgBox($MB_OK, "Message", "Done")
		ConsoleWrite(@WorkingDir)
		if (FileExists(@WorkingDir & "/node_modules")) Then
			#ConsoleWrite("exists nodemodules")
		Else
			RunWait(@ComSpec & " /c cd " & @WorkingDir & " & npm install")
			#ConsoleWrite("dont exists")
		EndIf
		RunWait(@ComSpec & " /c cd " & @WorkingDir & " & node server");
	Else
		MsgBox($MB_OK, "Message", $node_output)
		Exit;
	EndIf


EndFunc
#MsgBox($MB_OK, "Tutorial", "Hello World!")